import React from 'react';
import { Link } from 'react-router-dom';
import styles from './Character.module.css'

const Character = function(props){

    const {character} = props;

    return(
        <div className={styles.container}>
            <Link to={{
                pathname: '/profilepage',
                state: character }}>
                <img className={styles.profileImage} src={character.image} alt={character.name}/>
            </Link>
            <div className={styles.about}>
                <h5>{character.name}</h5>
                <p>{character.species}</p>
            </div>
        </div>
    )
}

export default Character;