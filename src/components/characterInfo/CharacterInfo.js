import React from 'react';
import styles from './CharacterInfo.module.css'

const CharacterInfo = function(props){

    const character = props.character;

    console.log(character)

    return(
        <div className={styles.info} >
            <p>Name: {character.name}</p>
            <p>Gender: {character.gender}</p>
            <p>Species: {character.species}</p>
            <p>Status: {character.status}</p>
            <p>Location: {character.location.name}</p>
            <p>Number of episodes: {character.episode.length}</p>
        </div>
    )
}

export default CharacterInfo;