import React, { useState, useEffect } from 'react';
import Character from '../character/Character.js';
import styles from './CharacterList.module.css';


const CharacterList = function(){

    let searchWord = "";

    const [characters, setCharacters] = useState([]);
    const [info, setInfo] = useState([])

    let url = `https://rickandmortyapi.com/api/character/?name=${searchWord}`;

    const [nextPage, setNextPage] = useState(url);


    useEffect(()=>{
        fetch(nextPage)
        .then(resp => resp.json())
        .then(resp => {
            if (resp.error){
                setCharacters([]);
            } else{
                const {results} = resp || [];
                setCharacters(results);
                setInfo(resp.info);
            }
        }).catch(err => console.error(err))
    }, [searchWord, nextPage])


    function nextPageHandle(event){
        setNextPage(info.next)
    }

    function prevPageHandler(event){
        setNextPage(info.prev)
    }

    var characterItems = characters.map(item => <Character character={item} key={item.id}/>)

    function searching(event){
        searchWord = event.target.value;
        setNextPage(`https://rickandmortyapi.com/api/character/?name=${searchWord}`)
    }

    return(
        <div>
            <div className={styles.inputField}>
            <input placeholder='Search character' onChange={searching}></input>
            </div>
                <div className={styles.buttons}>
            </div>
            <div id="character-list" className={styles.container}>
                {characterItems}
            </div>
            <div className={styles.buttons}>
                <button onClick={prevPageHandler}>Previous page</button>
                <button onClick={nextPageHandle}>Next page</button>
            </div>
        </div>
    )
}

export default CharacterList;