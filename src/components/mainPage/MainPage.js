import React from 'react';
import CharacterList from '../characterList/CharacterList.js';
import styles from './MainPage.module.css';

const MainPage = function() {

    return(
       <div>
           <div className={styles.header}>
                <h1>Rick &amp; Morty wiki</h1>
           </div>
            <CharacterList/>
       </div>        
    )
}

export default MainPage; 