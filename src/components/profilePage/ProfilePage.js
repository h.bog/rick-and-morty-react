import React from 'react';
import styles from './ProfilePage.module.css'
import CharacterInfo from '../characterInfo/CharacterInfo.js'
import { Link } from 'react-router-dom';

const ProfilePage = function(props){

    const character = props.location.state;

    return (
        <div>
            <h1 className={styles.header}>This is the profile page of {character.name}</h1>
            <div className={styles.container}>  
                <div className={styles.about}>          
                    <CharacterInfo character={character}/>
                </div>
                <div>
                    <img src={character.image} alt={character.name}/>
                </div>
            </div>
            <div className={styles.button}>
                <Link to={'/'}><button>Go back</button></Link>
            </div>
        </div>
    )

}


export default ProfilePage;