import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import MainPage from './components/mainPage/MainPage.js'
import ProfilePage from './components/profilePage/ProfilePage.js'

function App() {
  return (
    <Router>
      <div className="App">
        <main>
          <Switch>
            <Route exact path='/' component={MainPage}/>
            <Route path='/profilepage' component={ProfilePage}/>
          </Switch>
        </main>
      </div>
    </Router>
  );
}

export default App;

